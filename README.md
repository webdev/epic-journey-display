# EPIC Journey Website
Porter Libby, Dana Yao, Craig Earley

(Updated APR 2020 by Porter Libby)

[Full Dev Notes Document (USEFUL FOR GETTING STARTED)](https://docs.google.com/document/d/1df4I4jSINQF_vvuyl6I5YdxtMgdH7I4BaaKf1CJ1F4g/edit?usp=sharing)

## Project Layout
**public/css** - All the styles for the project are kept here. The files are named with the aspect that they control. This folder is also where font files are kept in the project.

- main.css - controls various random elements that dont fit under the other catagories. Also is last-loaded file, so it has the ability to override everything else.
- input.css - styles for inputs, radio buttons, checkboxes, etc.
- keyb.css - styles for on-screen keyboard for touchscreens.
- modal.css - styles for modal popup
- pdf.css - styles for PDF element to be rendered into the output pdf of a selection.

**public/data** - All the project data is contained in this folder. Currently there is just one file, `data.js`, which contains the dictionary for the data needed in the current implementation of the website.

**public/img** - All the images for the website should be stored here so that they can easily be referenced in HTML. One example of this is the slideshow on the main page of the website, which uses images from this folder.

**public/js** - All the javascript functionality for the website. It should be noted how this folder corrosponds to the `css` folder. This is due to the fact that most of the custom components for this project require both JS logic and CSS styles. As a result, files like `js/modal.js` and `css/modal.css` affect the same parts of the website.

## Local Work
The website is still static at this point, meaning that it doesn't have a server-side that needs to be run. The website can be opened simply by opening index.html in a web browser. 

The Earlham copy of the website exists here: https://epicjourney.cs.earlham.edu/

## PDF Export
The website allows exporting a PDF through the html2pdf library. The following element is the template for this PDF. The following can be controlled and modified by adjusting the `public/css/pdf.css`, and its contents can be changed by modifying the `generatePDF()` function in `public/js/main.js`.
```html
<div style='display:none; width: 100%; height: 100%;'>
    <div id='pdf_out'>
        <h>Your EPIC Journey Selection</h>
        <div id='pdf_content'></div>
    </div>
</div>
```

## Detail Modal
The modal has a simple structure within `index.html`. Its styles can be adjusted in the `public/css/modal.css` file. The functions which control the population of information into the modal are in `public/js/modal.js`, and are named in corrospondence with the sections they control.

```html
<div id="myModal" class="modal">
    <!-- Modal content -->
    <div id='modal_body' class="modal-content">
        <div style="display:inline-block; width: 100%;">

            <span id='close_btn'></span>
            <div id='modal_text'></div>
            <iframe id='modal-video'> </iframe>

        </div>
    </div>
</div>
```


