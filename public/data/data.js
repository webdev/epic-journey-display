// EPIC Journey Project - Data Dictionary
//
// January 2020
// Porter Libby, Dana Yao, Craig Earley


// This file contains a JSON-like structure of all the string data used in the EPIC journey website.
// Updating values will never break anything, just change what is displayed, updating keys can
// result in something breaking, so avoid when possible. 

// Each section in the website (1-4) has a different kind of content. Each has its own object in the dict below.

// Each entry in each section has all the information required to open a model of details

// URL - this field is a URL to external resources, probably to earlham.edu
// DESCRIPTION - text to be displayed in modal only
// TITLE - text to be used for the display title of the selection
// VIDEO URL - optional video URL if a video should be displayed in modal


var dictionary = {
    "major_selection": {
        "African and African American Studies": [
            "",
            "",
            "African and African American Studies",
            ""
        ],
        "Ancient and Classical Studies": [
            "",
            "",
            "Ancient and Classical Studies",
            ""
        ],
        "Archaeology": [
            "",
            "",
            "Archaeology",
            ""
        ],
        "Art": [
            "",
            "Our Art major at Earlham College is unique because it emphasizes both contemporary craft media, such as ceramics, metalsmithing and fiber art, and more traditional media, such as drawing, painting and photography. The Center for the Visual and Performing Arts opened in 2014. The $22 million facility offers separate studio spaces for photography, metals, ceramics, painting and drawing, and weaving, each wired for multi-media presentation; plus an art history classroom.Students are strongly encouraged to take advantage of the Great Lakes Colleges Association's New York Arts Program, which is an opportunity to apprentice with an artist or work as an intern in a major museum or gallery.",
            "Art",
            ""
        ],
        "Athletics Wellness and Physical Education": [
            "",
            "",
            "Athletics Wellness and Physical Education",
            ""
        ],
        "Biochemistry": [
            "",
            "",
            "Biochemistry",
            ""
        ],
        "Biology": [
            "",
            "",
            "Biology",
            ""
        ],
        "Chemistry": [
            "",
            "",
            "Chemistry",
            ""
        ],
        "Chinese Languages" :[
            "",
            "",
            "Chinese Languages",
            ""
        ],
        "Chinese Studies": [
            "",
            "",
            "Chinese Studies",
            ""
        ],
        "Comparative Languages and Linguistics": [
            "",
            "",
            "Comparative Languages and Linguistics",
            ""
        ],
        "Computer Science": [
            "https://earlham.edu/computer-science/",
            "Built in 2015, the Center for Science and Technology includes a state-of-the-art computer lab and a robotics lab, supporting  both what we teach and how we teach it. Student/faculty research opportunities in Computer Science have been enriched with involved faculty, including Xunfei Jiang's work on power saving scheduling algorithms and Charlie Peck work with students on field science projects. Our Green Science group designed and built our first solar charging station. Now anyone in the community can charge their devices using solar energy! Given Earlham’s globally diverse community, most students don’t even have to leave campus in order to converse with native speakers in the languages they are studying.",
            "Computer Science",
            ""
        ],
        "East Asian Studies": [
            "",
            "",
            "East Asian Studies",
            ""
        ],
        "Economics": [
            "",
            "",
            "Economics",
            ""
        ],
        "English": [
            "",
            "",
            "English",
            ""
        ],
        "Environmental Sustainability": [
            "",
            "",
            "Environmental Sustainability",
            ""
        ],
        "Film Studies": [
            "",
            "",
            "Film Studies",
            ""
        ],
        "French and Francophone Studies": [
            "",
            "",
            "French and Francophone Studies",
            ""
        ],
        "Geology": [
            "",
            "",
            "Geology",
            ""
        ],
        "German Language and Literature": [
            "",
            "",
            "German Language and Literature",
            ""
        ],
        "Global Management": [
            "",
            "",
            "Global Management",
            ""
        ],
        "History": [
            "",
            "",
            "History",
            ""
        ],
        "Human Development and Social Relations": [
            "",
            "",
            "Human Development and Social Relations",
            ""
        ],
        "International Studies": [
            "",
            "",
            "International Studies",
            ""
        ],
        "Japanese Language and Linguistics": [
            "",
            "",
            "Japanese Language and Linguistics",
            ""
        ],
        "Japanese Studies": [
            "",
            "",
            "Japanese Studies",
            ""
        ],
        "Jewish Studies": [
            "",
            "",
            "Jewish Studies",
            ""
        ],
        "Languages and Cultures": [
            "",
            "",
            "Languages and Cultures",
            ""
        ],
        "Master of Arts in Teaching": [
            "",
            "",
            "Master of Arts in Teaching",
            ""
        ],
        "Master of Education": [
            "",
            "",
            "Master of Education",
            ""
        ],
        "Mathematics": [
            "",
            "",
            "Mathematics",
            ""
        ],
        "Museum Studies": [
            "",
            "",
            "Museum Studies",
            ""
        ],
        "Music": [
            "",
            "",
            "Music" ,
            ""
        ],
        "Neuroscience": [
            "",
            "",
            "Neuroscience",
            ""
        ],
        "Peace and Global Studies": [
            "",
            "",
            "Peace and Global Studies",
            ""
        ],
        "Philosophy": [
            "",
            "",
            "Philosophy",
            ""
        ],
        "Physics and Astronomy": [
            "",
            "",
            "Physics and Astronomy",
            ""
        ],
        "Politics": [
            "",
            "",
            "Politics",
            ""
        ],
        "Psychology": [
            "",
            "",
            "Psychology",
            ""
        ],
        "Public Health": [
            "",
            "",
            "Public Health",
            ""
        ],
        "Public Policy": [
            "",
            "",
            "Public Policy",
            ""
        ],
        "Religion": [
            "",
            "",
            "Religion",
            ""
        ],
        "Sociology and Anthropology": [
            "",
            "",
            "Sociology and Anthropology",
            ""
        ],
        "Spanish and Hispanic Studies": [
            "",
            "",
            "Spanish and Hispanic Studies",
            ""
        ],
        "Teaching English to Speakers of Other Languages": [
            "",
            "",
            "Teaching English to Speakers of Other Languages",
            ""
        ],
        "Theatre Arts": [
            "",
            "",
            "Theatre Arts",
            ""
        ],
        "Women's Gender Sexuality Studies": [
            "",
            "",
            "Women's Gender Sexuality Studies",
            ""
        ]
    },
    "passion_project": {
        'entrepreneurship_innovation' : [
            "",
            "The Center for Entrepreneurship and Innovation helps students connect business to the social good. Join the EPIC Grand Challenge, where you’ll build a plan to impact 25,000 people in the Richmond area in the areas of neighborhood development, employability, and community development. Or get involved with our campus chapter of Net Impact, a student organization that serves as a leadership accelerator for future changemakers.",
            "Center for Entrepreneurship & Innovation",
            "https://www.youtube.com/embed/OFXvx_QAbX4?list=PLLskAD3RU43XcMiIvtAEOnTHh0Zdp33N1"
        ],
        'environmental_leadership' : [
            "",
            "The Center for Environmental Leadership is for students interested in being outside, getting their hands dirty, or tackling environmental challenges on campus. Join the Student Sustainability Corps, learn about sustainable agriculture on Miller Farm, or take trips into nature and gain invaluable leadership skills through Outdoor Education.",
            "Environmental Leadership",
            "https://www.youtube.com/embed/OFXvx_QAbX4?list=PLLskAD3RU43XcMiIvtAEOnTHh0Zdp33N1"
        ],
        'global_health' : [
            "",
            "The Center for Global Health is for students interested in health careers. Get hands-on experience in the healthcare field through the CGH health externship program, study public health issues in the Dominican Republic alongside your faculty, and join our campus chapter of HOSA (Health Occupations Students of America).",
            "Gobal Health",
            "https://www.youtube.com/embed/a4vlCzImZVs?list=PLLskAD3RU43XcMiIvtAEOnTHh0Zdp33N1"
        ],
        'social_justice' : [
            "",
            "The Center for Social Justice is for students interested in activism and advocacy. Apply for the Earlham Peace Prize, which provides students with $10,000 to do a peace project anywhere in the world, or participate in Spring Lobby Weekend in Washington, D.C., where you’ll advocate for social justice with our federal lawmakers.",
            "Social Justice",
            "https://www.youtube.com/embed/oYhn3XP1NmA"
        ]
    },
    "access_opportunities": {
        'internships' : [
            "https://earlham.edu/center-for-career-and-community-engagement/internship-program/",
            "The Earlham Internship Program is a structure within the Center for Global and Career Education that supports the practical application of knowledge gained in a classroom to a real-world, professional setting in the form of summer internships. Students may participate in the program for credit as well as educational and professional support.",
            "Internships",
            "https://www.youtube.com/embed/S_sZWo9-35w",
        ],
        'research' : [
            "",
            "Sample Description",
            "Research",
            ""
        ],
        'off_campus_study' : [
            "",
            "Off-campus study is a central part of an Earlham education. We encourage you to become one of the many Earlham students who take advantage of these challenging and enriching educational experiences. More than 65 percent of Earlham graduates participate in a semester or year-long program during their Earlham career. Students tell us that during an off-campus experience they learn new ways of living and thinking, new ways of understanding the world, and they learn more about themselves and their own culture. Past participants often report that their off-campus experience was one of the most important parts of their liberal arts education and one that has a life-long impact on them.",
            "Off-Campus Study",
            "https://www.youtube.com/embed/5f75RW0pMvY"
        ]
    },
    "ways_forward": {
        "carrie_seltzer": [
            "",
            '"I think my Earlham education allowed me to grow into my true self. I didn’t feel like I had to pretend to be someone who I wasn’t. And that for me was an incredible, sort of personal awakening."',
            "Carrie Seltzer, the Digital Naturalist",
            ""
        ],
        "oneido_luis": [
            "",
            '"I think the great thing about an Earlham education is, by design, that it is versatile, it is dynamic, it is full of inquiry and, as cheesy as it sounds, it’s also quite inspiring."',
            "Oneido Luis, the Community Connector",
            ""
        ],
        "sonat_hart": [
            "",
            '"An Earlham education is incredibly beneficial because it really teaches you what it takes to succeed."',
            "Sonat Hart, the Gold Medal Distiller",
            ""
        ]
    }
}