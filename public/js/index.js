// GLOBALS
var email_href = "";
var selection_tracking = [[],[],[],[]];

var color_rotate = 0; // controls which color will be used first
var color_arr = [ // list of earlham approved colors for buttons
    "#B9771E",
    "#F9423A",
    "#4A8096",
    "#9B1027"
]

function random_ec_color(){
    current_color = color_arr[color_rotate];
    color_rotate++;
    if (color_rotate > 3){
        color_rotate = 0;
    }
    return current_color;
}

function create_email_href(){
    // create a HREF mailto link so that the selection can be sent as an email
    if (selection_tracking[0] != "" && selection_tracking[1] != "" && selection_tracking[2] != "" && selection_tracking[3] != ""){
        email_href = "mailto:example@example.com?subject=My EPIC Journey&body=";
        email_href += "[NAME]%0D%0A%0D%0A";
        email_href += "Official Earlham EPIC Journey Results: %0D%0A";

        email_href += "1. Major Selection: " + selection_tracking[0] + "%0D%0A";
        email_href += "2. Passion Project Selection: " + selection_tracking[1] + "%0D%0A";
        email_href += "3. Access opportunities Selection: " + selection_tracking[2] + "%0D%0A";
        email_href += "4. Explore Ways Forward: " + selection_tracking[3] + "%0D%0A";
    
        var email_elem = document.getElementById('email_out');
        email_elem.href = email_href;
        return true;
    }else{
        return false;
    }
}

function export_email(){
    if (create_email_href()){
        window.location.href = email_href;
    }
    else{
        alert("Please make a selection for each section to export your results!");
    } 
}

function generatePDF() {
    const element = document.getElementById("pdf_out");
    const content = document.getElementById("pdf_content");
    if (selection_tracking[0] != "" && selection_tracking[1] != "" && selection_tracking[2] != "" && selection_tracking[3] != ""){

        content.innerHTML = "<span id='pdf_heading'>1. Major Selection </span><br>";
        for (x=0;x<selection_tracking[0].length;x++){
            content.innerHTML += "<span id='pdf_item'>&emsp;" + dictionary["major_selection"][selection_tracking[0][x]][2] + "</span><br>"
        }

        content.innerHTML += "<br><span id='pdf_heading'>2. Passion Project Selection </span><br>";
        for (x=0;x<selection_tracking[1].length;x++){
            content.innerHTML += "<span id='pdf_item'>&emsp;" + dictionary["passion_project"][selection_tracking[1][x]][2] + "</span><br>"
        }

        content.innerHTML += "<br><span id='pdf_heading'>3. Access opportunities Selection </span><br>";
        for (x=0;x<selection_tracking[2].length;x++){
            content.innerHTML += "<span id='pdf_item'>&emsp;" + dictionary["access_opportunities"][selection_tracking[2][x]][2] + "</span><br>"
        }

        content.innerHTML += "<br><span id='pdf_heading'>4. Explore Ways Forward </span><br>";
        for (x=0;x<selection_tracking[3].length;x++){
            content.innerHTML += "<span id='pdf_item'>&emsp;" + dictionary["ways_forward"][selection_tracking[3][x]][2] + "</span><br>"
        }
    
        html2pdf().from(element).save("My EPIC Journey"); // create pdf
        return true;
    }else{
        alert("Please make a selection for each section to export your results!");
        return false;
    }
}







