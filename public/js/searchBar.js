// Render search bar
// Porter Libby 2019


function create_searchbar(parent, key){

    //create search obj
    var search_container = document.createElement("div");
    search_container.id = key + '-search-container';
    search_container.classList = "";
    var search = document.createElement("INPUT");
    search.id = 'searchbar'
    search.placeholder = "e.g. Computer Science";
    search.classList = "";
    search.setAttribute("type", "text");
    search.setAttribute("value", "");

    prompt = document.createElement('span');
    // prompt.innerHTML = 'Search for ' + key + ':';

    search_container.appendChild(prompt);
    search_container.appendChild(search);
    parent.appendChild(search_container);
    names_for_search = Object.keys(dictionary[key]);

    autocomplete(parent.querySelector("#searchbar"), names_for_search,dictionary[key]);
}

function autocomplete(inp, arr, sub_dict) {
    var currentFocus;
    inp.addEventListener("input", function(e) {
        if (arr.includes(inp.value)){
            createInfo(sub_dict[inp.value])
        }else{
            destroyInfo();
        }
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        this.parentNode.appendChild(a);
        for (i = 0; i < arr.length; i++) {
            if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                b = document.createElement("DIV");
                b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i].substr(val.length);
                b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                b.addEventListener("click", function(e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) { //up
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });
    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }
    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }
    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
        if (arr.includes(inp.value)){
            if (!selection_tracking[0].includes(inp.value)){
                createInfo(sub_dict[inp.value], inp.value)
            }
        }else{
            destroyInfo()
        }
    });
}

function createInfo(info_arr){
    console.log(info_arr);
    selection_tracking[0].push(info_arr[2]); // set value for export
    set_modal_text(info_arr[0], info_arr[1], info_arr[2], info_arr[3])
    open_modal(random_ec_color());
}
function destroyInfo(){
    console.log('No info')
}