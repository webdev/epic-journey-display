// This file contains functions concerned with operating the details modal

// this function handles the events for when the modal IS open and should be closed.
$(document).ready(function() {
    var modal = document.getElementById("myModal");
    var close_btn = document.getElementById("close_btn")
    window.onclick = function(event) {
        console.warn("CLOSING MODAL")
        console.log(event.target)
        if (event.target == modal) {
            modal.style.display = "none";
        }
        if (event.target == close_btn) {
            modal.style.display = "none";
        }
    }
});

// this function handles opening the modal when it IS closed.
function open_modal(color) {
    console.warn('OPENING MODAL')
    var modal = document.getElementById("myModal");
    modal.style.display = "block";
    document.getElementById("modal_body").style.backgroundColor = color;
}

// this function sets the content of the model. (INCOMPLETE)
function set_modal_text(url, description, title, video_url) {
    var text_obj = document.getElementById('modal_text');
    var video_obj = document.getElementById('modal-video');

    text_obj.innerHTML = "<strong style='font-size: 20px;'>" + title + "</strong>";
    text_obj.innerHTML += "<br><br>";
    text_obj.innerHTML += description;
    text_obj.innerHTML += "<br><br>";
    if (url){
        text_obj.innerHTML += "<span>See more info about this subject <mark><a href='" + url + "'>here</a></mark></span>";
    }

    if (video_url == ""){
        video_obj.style.display = "none";
    }else{
        video_obj.style.display = "block";
        video_obj.src = video_url;
    }
}

// OPEN MODAL FOR SPECIFIC BLOCK

function section_two(val, c){ // load info for section two based on value
    if (selection_tracking[1].includes(val)){
        // UNSELECT
        console.log(selection_tracking)
        var index = selection_tracking[1].indexOf(val);
        if (index > -1) {
            selection_tracking[1].splice(index, 1);
        }
        console.log(selection_tracking)
    }
    else{
        // SELECT
        var url = dictionary["passion_project"][val][0];
        var desc = dictionary["passion_project"][val][1];
        var title = dictionary["passion_project"][val][2];
        var video = dictionary["passion_project"][val][3];
        selection_tracking[1].push(val); // set value for export
        open_modal($('label[for="'+c+'"]')[0].style.backgroundColor)
        set_modal_text(url,desc,title,video)
    }
}

function section_three(val, c){ // load info for section three based on value
    if (selection_tracking[2].includes(val)){
        // UNSELECT
        console.log(selection_tracking)
        var index = selection_tracking[2].indexOf(val);
        if (index > -1) {
            selection_tracking[2].splice(index, 1);
        }
        console.log(selection_tracking)
    }
    else{
        // SELECT
        var url = dictionary["access_opportunities"][val][0];
        var desc = dictionary["access_opportunities"][val][1];
        var title = dictionary["access_opportunities"][val][2];
        var video = dictionary["access_opportunities"][val][3];
        selection_tracking[2].push(val); // set value for export
        open_modal($('label[for="'+c+'"]')[0].style.backgroundColor)
        set_modal_text(url,desc,title,video)
    }
}

function section_four(val, c){ // load info for section two based on value
    if (selection_tracking[3].includes(val)){
        // UNSELECT
        console.log(selection_tracking)
        var index = selection_tracking[3].indexOf(val);
        if (index > -1) {
            selection_tracking[3].splice(index, 1);
        }
        console.log(selection_tracking)
    }
    else{
        // SELECT
        var url = dictionary["ways_forward"][val][0];
        var desc = dictionary["ways_forward"][val][1];
        var title = dictionary["ways_forward"][val][2];
        var video = dictionary["ways_forward"][val][3];
        selection_tracking[3].push(val); // set value for export
        open_modal($('label[for="'+c+'"]')[0].style.backgroundColor)
        set_modal_text(url,desc,title,video)
    }
}