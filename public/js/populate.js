// For populating each section with information from DATA.JS
$(document).ready(function() { // populate when document is loaded

    // SECTION 2
    var form = document.getElementById('radio-form-2');
    var count = 0;
    for (property in dictionary["passion_project"]) {
        // for each item in passion project section, create a form element
        count++;
        form.innerHTML += '<input type="checkbox" id="' + "passion-project-" + count + '" name="passion-project" value="' + property + '" onchange="section_two(this.value, this.id)">';
        form.innerHTML += '<label style="border: 1px solid black; background-color:' + random_ec_color()  + ';" for="' + "passion-project-" + count + '">' + dictionary["passion_project"][property][2] + '</label><br>';
    }
    
    // SECTION 3
    var form = document.getElementById('radio-form-3');
    var count = 0;
    color_rotate = 0; // reset color cycle for next block
    for (property in dictionary["access_opportunities"]) {
        // for each item in passion project section, create a form element
        count++;
        form.innerHTML += '<input type="checkbox" id="' + "access_opportunities-" + count + '" name="access_opportunities" value="' + property + '" onchange="section_three(this.value,this.id)">';
        form.innerHTML += '<label style="border: 1px solid black; background-color:' + random_ec_color() + ';" for="' + "access_opportunities-" + count + '">' + dictionary["access_opportunities"][property][2] + '</label><br>';
    }

    // SECTION 4
    var form = document.getElementById('radio-form-4');
    var count = 0;
    color_rotate = 0; // reset color cycle for next block
    for (property in dictionary["ways_forward"]) {
        // for each item in passion project section, create a form element
        count++;
        form.innerHTML += '<input type="checkbox" id="' + "ways_forward-" + count + '" name="ways_forward" value="' + property + '" onchange="section_four(this.value, this.id)">';
        form.innerHTML += '<label  style="border: 1px solid black; background-color:' + random_ec_color() + ';"for="' + "ways_forward-" + count + '">' + dictionary["ways_forward"][property][2] + '</label><br>';
    }
});